import 'package:cloud_firestore/cloud_firestore.dart';

import '../entity/sale.dart';

class SaleRepository {
  late final CollectionReference _collection;

  SaleRepository() {
    _collection = FirebaseFirestore.instance.collection("sales");
  }

  Future<void> newSale(SaleEntity sale) async {
    await _collection
        .withConverter<SaleEntity>(
            fromFirestore: SaleEntity.fromFirestore,
            toFirestore: (value, _) => value.toFirestore())
        .add(sale);
  }

  Future<List<SaleEntity>> getAllByUserId(String id) async {
    var query = await _collection
        .where("user", isEqualTo: id)
        // .orderBy("name")
        .withConverter<SaleEntity>(
            fromFirestore: SaleEntity.fromFirestore,
            toFirestore: (value, _) => value.toFirestore())
        .get();

    var sales = query.docs.cast().map<SaleEntity>((e) {
      var sale = e.data();
      sale.id = e.id;
      return sale;
    });

    // var lista = sales.toList();
    // lista.sort(
    //   (a, b) =>
    //       a.clientName!.toLowerCase().compareTo(b.clientName!.toLowerCase()),
    // );
    // return lista;

    // return sales.toList()
    //   ..sort((a, b) =>
    //       a.clientName!.toLowerCase().compareTo(b.clientName!.toLowerCase()));

    return sales.toList();
  }

  Future<SaleEntity> getById(String id) async {
    var snapshot = await _collection
        .doc(id)
        .withConverter<SaleEntity>(
            fromFirestore: SaleEntity.fromFirestore,
            toFirestore: (value, _) => value.toFirestore())
        .get();

    var sale = snapshot.data();
    if (sale == null) {
      return Future.error("No existe una venta con id: $id");
    }

    return sale;
  }

  Future<void> update(SaleEntity sale) async {
    await _collection
        .doc(sale.id)
        .withConverter<SaleEntity>(
            fromFirestore: SaleEntity.fromFirestore,
            toFirestore: (value, _) => value.toFirestore())
        .set(sale);
  }
}
