import '../model/entity/sale.dart';
import '../model/repository/backend.dart';
import '../model/repository/fb_storage.dart';
import '../model/repository/sale.dart';

class SaleController {
  late SaleRepository _repository;
  late FirebaseStorageRepository _storageRepository;
  late BackendRepository _backendRepository;

  SaleController() {
    _repository = SaleRepository();
    _storageRepository = FirebaseStorageRepository();
    _backendRepository = BackendRepository();
  }

  Future<void> save(SaleEntity sale) async {
    // Si la venta trae foto de cliente
    if (sale.photo != null) {
      // cargo la foto en storage
      var url = await _storageRepository.loadFile(sale.photo!, "sale/photo");
      // cambio direccion foto por la del storage
      sale.photo = url;
    }

    await _repository.newSale(sale);
  }

  Future<void> updatePhoto(String id, String filePath) async {
    var url = await _storageRepository.loadFile(filePath, "sale/photo");

    var sale = await _repository.getById(id);
    sale.photo = url;
    await _repository.update(sale);
  }

  Future<List<SaleEntity>> listAll(String id) async {
    return await _repository.getAllByUserId(id);
  }

  Future<int> calculateValue(int amount, int parts, String periodicity) async {
    if (amount <= 0) {
      return Future.error("El monto no es válido");
    }
    if (parts <= 0) {
      return Future.error("El número de cuotas no es válido");
    }
    if (periodicity.isEmpty) {
      return Future.error("La periodicidad no se asignó de manera correcta");
    }

    var value =
        await _backendRepository.calculateValue(amount, parts, periodicity);

    return value;
  }
}
