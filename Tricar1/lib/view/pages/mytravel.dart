import 'package:flutter/material.dart';
import 'newtravel.dart';
import '../widgets/drawer.dart';

class MytravelPage extends StatelessWidget {
  final String email;
  final String name;

  const MytravelPage({super.key, required this.email, required this.name});

  @override
  Widget build(BuildContext context) {
    final lista = _listarViajes();

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purple,
        title: const Text("Mis Viajes"),
      ),
      drawer: const DrawerWidget(),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              "Historial",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: lista.length,
                itemBuilder: (context, index) => ListTile(
                  leading: const CircleAvatar(
                    backgroundColor: Colors.orange,
                  ),
                  title: Text(lista[index]),
                  subtitle: const Text("ubicación"),
                  trailing: IconButton(
                    icon: const Icon(Icons.phone),
                    onPressed: () {
                      // TODO Realizar la llamada Telefonica
                    },
                  ),
                  onTap: () {
                    // TODO: Ir a la ventana detalle del cobro
                  },
                ),
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add_shopping_cart),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const NewTravel(),
            ),
          );
        },
      ),
    );
  }

  List<String> _listarViajes() {
    //TODO: Traer la lista de cobros del dia de la BD

    return List<String>.generate(
      20,
      (index) => "Viaje ${index + 1}",
    );
  }
}
