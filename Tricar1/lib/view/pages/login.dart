import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tricar_1/view/pages/newtravel.dart';
import 'package:tricar_1/view/pages/register.dart';
import '../../controller/login.dart';
import '../../controller/request/login.dart';

class LoginPage extends StatelessWidget {
  final _pref = SharedPreferences.getInstance();

  final _imageUrl = "assets/images/Logo2001px.png";
  late final LoginController _controller;
  late final LoginRequest _request;

  LoginPage({super.key}) {
    _controller = LoginController();
    _request = LoginRequest();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              _logo(),
              const Text(
                "¡Hola!",
                style: TextStyle(fontSize: 18),
              ),
              const SizedBox(
                height: 10,
              ),
              _formulario(context),
              _inicioAlternativo(),
              TextButton(
                style: TextButton.styleFrom(
                  foregroundColor: Colors.purple,
                ),
                child: const Text("¿Sin cuenta? Crea una aquí"),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => RegisterPage()),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _logo() {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Image.asset(_imageUrl),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }

  Widget _formulario(BuildContext context) {
    final formKey = GlobalKey<FormState>();

    return Form(
      key: formKey,
      child: Column(
        children: [
          _campoCorreoElectronico(),
          const SizedBox(height: 8),
          _campoClave(),
          const SizedBox(height: 20),
          _Ingresar(context, formKey),
          const SizedBox(
            height: 8,
          ),
        ],
      ),
    );
  }

  Widget _campoCorreoElectronico() {
    return TextFormField(
      maxLength: 50,
      keyboardType: TextInputType.emailAddress,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Correo Electrónico',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El correo electronico es obligatorio";
        }
        if (!value.contains("@") || !value.contains(".")) {
          return "El correo tiene un formato inválido";
        }
        return null;
      },
      onSaved: (value) {
        _request.email = value!;
      },
    );
  }

  Widget _campoClave() {
    return TextFormField(
      maxLength: 30,
      obscureText: true,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Contraseña',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "La contraseña es obligatoria";
        }
        if (value.length < 6) {
          return "Mínimo debe contener 6 caracteres";
        }
        return null;
      },
      onSaved: (value) {
        _request.password = value!;
      },
    );
  }

  Widget _inicioAlternativo() {
    return Column(
      children: [
        const Text("O continua con"),
        const SizedBox(
          height: 8,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            /*IconButton(
              icon: const Icon(Icons.facebook),
              iconSize: 50,
              color: Color.fromARGB(255, 37, 11, 234),
              onPressed: () {},
            ),*/
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                foregroundColor: Colors.black,
                backgroundColor: const Color.fromARGB(255, 37, 11, 234),
              ),
              onPressed: () {},
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 3, 0, 3),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Icon(
                      Icons.facebook,
                      size: 30,
                      color: Color.fromARGB(255, 255, 255, 255),
                    ),

                    //IconTheme(data: Color.fromARGB(255, 255, 255, 255),
                    //child: <Widget> Size(width50, height:30)),
                    /*Image(
                      image: AssetImage("assets/images/googlelogo.png"),
                      height: 30,
                      width: 50,
                    ),*/
                    Padding(
                      padding: EdgeInsets.only(left: 10, right: 8),
                      child: Text(
                        'Facebook',
                        style: TextStyle(
                          fontSize: 12,
                          color: Color.fromARGB(255, 255, 255, 255),
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            /*ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: Color.fromARGB(255, 37, 11, 234),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 3),
                  textStyle: const TextStyle(
                      fontSize: 12, fontWeight: FontWeight.bold)),
              child: const Text("Facebook"),
              onPressed: () {},
            ),*/
            const Text(" o"),
            /*IconButton(
              icon: const Icon(Icons.email_outlined),
              iconSize: 50,
              color: Color.fromARGB(255, 234, 82, 11),
              onPressed: () {},
            ),*/
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                foregroundColor: Colors.black,
                backgroundColor: Colors.white,
              ),
              onPressed: () {},
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 3, 0, 3),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Image(
                      image: AssetImage("assets/images/googlelogo.png"),
                      height: 30,
                      width: 50,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 10, right: 8),
                      child: Text(
                        'Google',
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.black54,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            /* ElevatedButton(
              child: const Text("Google"),
              onPressed: () {},
            ),*/
          ],
        ),
        const SizedBox(
          height: 5,
        ),
      ],
    );
  }

  Widget _Ingresar(BuildContext context, GlobalKey<FormState> formKey) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
          backgroundColor: Colors.purple,
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
          textStyle:
              const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      child: const Text('Ingresar'),
      onPressed: () async {
        if (formKey.currentState!.validate()) {
          formKey.currentState!.save();

          // Validar correo y clave en BD
          try {
            final nav = Navigator.of(context);

            var userInfo = await _controller.validateEmailPassword(_request);

            var pref = await _pref;
            pref.setString("uid", userInfo.id!);
            pref.setString("name", userInfo.name!);
            pref.setString("email", userInfo.email!);
            pref.setBool("admin", userInfo.isAdmin!);
            if (userInfo.photo != null) {
              pref.setString("photo", userInfo.photo!);
            }

            nav.pushReplacement(MaterialPageRoute(
              builder: (context) => const NewTravel(),
            ));
          } catch (e) {
            // showDialog(
            //   context: context,
            //   builder: (context) => AlertDialog(
            //     title: const Text("Ventas"),
            //     content: Text(e.toString()),
            //   ),
            // );

            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text(e.toString())));
          }
        }
      },
    );
  }
}
