import 'package:flutter/material.dart';

import '../widgets/drawer.dart';

void main() {
  runApp(const RentcarPage());
}

class RentcarPage extends StatelessWidget {
  const RentcarPage({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.purple,
            bottom: const TabBar(
              tabs: [
                Tab(icon: Icon(Icons.directions_car)),
                Tab(icon: Icon(Icons.directions_transit)),
                Tab(icon: Icon(Icons.directions_bike)),
              ],
            ),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/images/mapa1.png',
                  fit: BoxFit.contain,
                  height: 32,
                ),
                const Text("Alquiler"),
              ],
            ),
          ),
          body: const TabBarView(
            children: [
              Icon(Icons.directions_car),
              Icon(Icons.directions_transit),
              Icon(Icons.directions_bike),
            ],
          ),
          drawer: const DrawerWidget(),
        ),
      ),
    );
  }
}
