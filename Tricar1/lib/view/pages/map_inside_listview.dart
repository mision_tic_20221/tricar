import 'dart:js';

import 'package:flutter/material.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:latlong2/latlong.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tricar_1/view/pages/zoombuttons_plugin_option.dart';

import '../../controller/sales.dart';
import '../../model/entity/sale.dart';
import '../widgets/drawer.dart';
import '../widgets/photo_avatar.dart';

class MapInsideListViewPage extends StatelessWidget {
  final _pref = SharedPreferences.getInstance();
  late final SaleEntity _sale;
  late final SaleController _controller;
  late final PhotoAvatarWidget _avatar;

  MapInsideListViewPage({Key? key}) : super();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.purple, title: const Text('Nueva Carrera')),
      drawer: const DrawerWidget(),
      body: Padding(
        padding: const EdgeInsets.all(8),
        child: ListView(
          scrollDirection: Axis.vertical,
          children: [
            SizedBox(
              height: 300,
              child: FlutterMap(
                options: MapOptions(
                  absorbPanEventsOnScrollables: true,
                  center: LatLng(3.93, -76.4),
                  zoom: 5,
                ),
                children: [
                  TileLayer(
                    urlTemplate:
                        'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
                    userAgentPackageName: 'dev.fleaflet.flutter_map.example',
                  ),
                  const FlutterMapZoomButtons(
                    minZoom: 4,
                    maxZoom: 19,
                    mini: true,
                    padding: 10,
                    alignment: Alignment.bottomLeft,
                  ),
                ],
              ),
            ),
            const Card(
              child: ListTile(
                  title: Text(
                'Te llevamos a donde quieras ir',
                textAlign: TextAlign.center,
                selectionColor: Colors.orange,
              )),
            ),
            const SizedBox(height: 8),
            _formulario(context),
            const Card(
                child: ListTile(
                    title: Text(
              'Buen viaje',
              textAlign: TextAlign.center,
              selectionColor: Colors.orange,
            )))
          ],
        ),
      ),
    );
  }

  Widget _formulario(BuildContext context) {
    final formKey = GlobalKey<FormState>();

    return Form(
      key: formKey,
      child: Column(
        children: [
          _campoDireccionRecogida(),
          const SizedBox(height: 8),
          _campoDireccionDestino(),
          const SizedBox(height: 8),
          _campoMonto(),
          const SizedBox(height: 8),
          Row(
            children: [
              _tipoTransporte(),
              const SizedBox(width: 8),
              _numeroPasajeros(),
            ],
          ),
          const SizedBox(height: 8),
          _detalle(),
          const SizedBox(height: 8),
          _botonVamos(context, formKey),
        ],
      ),
    );
  }

  Widget _campoDireccionRecogida() {
    return TextFormField(
      keyboardType: TextInputType.streetAddress,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Te recogemos en:',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El campo es obligatorio";
        }
        return null;
      },
    );
  }

  Widget _campoDireccionDestino() {
    return TextFormField(
      keyboardType: TextInputType.streetAddress,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Te llevamos a:',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El campo es obligatorio";
        }
        return null;
      },
    );
  }

  Widget _campoMonto() {
    return TextFormField(
      initialValue: "0",
      textAlign: TextAlign.right,
      keyboardType: TextInputType.number,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Tu oferta: ',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El campo es obligatorio";
        }
        if (int.parse(value) < 1) {
          return "El valor no es válido";
        }
        return null;
      },
    );
  }

  Widget _numeroPasajeros() {
    return Expanded(
      child: TextFormField(
        initialValue: "1",
        textAlign: TextAlign.right,
        keyboardType: TextInputType.number,
        decoration: const InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Número de pasajeros',
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "El campo es obligatorio";
          }
          if (int.parse(value) < 1) {
            return "El valor no es válido";
          }
          return null;
        },
      ),
    );
  }

  Widget _tipoTransporte() {
    var opciones = <String>["Moto", "Trimoto", "Carro", "Jeep"];
    var valor = opciones[1];

    return Expanded(
      child: DropdownButtonFormField(
        value: valor,
        decoration: const InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'En qué vas:',
        ),
        items: opciones
            .map<DropdownMenuItem<String>>(
                (String value) => DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    ))
            .toList(),
        onChanged: (value) {},
      ),
    );
  }

  Widget _detalle() {
    return TextFormField(
      keyboardType: TextInputType.text,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Detalle:',
      ),
    );
  }

  Widget _botonVamos(BuildContext context, GlobalKey<FormState> formKey) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(backgroundColor: Colors.purple),
      child: const Text("Vamos"),
      onPressed: () async {
        if (formKey.currentState!.validate()) {
          formKey.currentState!.save();
          _sale.photo = _avatar.photo;
          // Guardar los datos en la BD
          try {
            final mess = ScaffoldMessenger.of(context);
            final nav = Navigator.of(context);

            // Captura la informacion de la ubicacion del usuario
            await _getGPSLocation();

            await _controller.save(_sale);

            mess.showSnackBar(
              const SnackBar(
                content: Text("Viaje registrado con éxito"),
              ),
            );

            // Volver a la pantalla anterior
            nav.pop();
          } catch (e) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text("Error: $e"),
              ),
            );
          }
        }
      },
    );
  }

  Future<void> _getGPSLocation() async {
    final location = Location();

    var serviceEnabled = await location.serviceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await location.requestService();
      if (!serviceEnabled) {
        return;
      }
    }

    var permission = await location.hasPermission();
    if (permission == PermissionStatus.denied) {
      permission = await location.requestPermission();
      if (permission != PermissionStatus.granted) {
        return;
      }
    }

    final locationData = await location.getLocation();
    _sale.lat = locationData.latitude;
    _sale.lng = locationData.longitude;
  }
}
