import 'package:flutter/material.dart';

import '../widgets/drawer.dart';

class OtherCityPage extends StatelessWidget {
  const OtherCityPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purple,
        title: const Text("A Otra Ciudad"),
      ),
      drawer: const DrawerWidget(),
    );
  }
}
