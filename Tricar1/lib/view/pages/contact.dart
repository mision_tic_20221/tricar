import 'package:flutter/material.dart';

import '../widgets/drawer.dart';

class ContactPage extends StatelessWidget {
  const ContactPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purple,
        title: const Text("Contacto"),
      ),
      drawer: const DrawerWidget(),
    );
  }
}
