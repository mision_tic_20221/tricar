import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../controller/sales.dart';
import '../../model/entity/sale.dart';
import '../widgets/drawer.dart';
import 'newtravel.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<SaleEntity> _lista = [];
  final _pref = SharedPreferences.getInstance();
  final _saleController = SaleController();

/*class HomePage extends StatelessWidget {
    final String email;
  final String name;

  const HomePage ({super.key, required this.email, required this.name});*/

  /*@override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purple,
        title: const Text("Nueva Carrera"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(3.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset("assets/images/mapa1.png"),
            const Text(
              "Mapa",
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            Image.asset("assets/images/mapa1.png")
          ],
        ),
      ),
      drawer: const DrawerWidget(),
    );
  }
}*/

  @override
  void initState() {
    super.initState();
    _listarCobros();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.purple,
          title: const Text("Nueva Carrera"),
        ),
        drawer: const DrawerWidget(),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                "Cobros",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Expanded(
                child: ListView.builder(
                    itemCount: _lista.length,
                    itemBuilder: (context, index) {
                      final sale = _lista[index];

                      final CircleAvatar avatar;
                      if (sale.photo != null) {
                        avatar = CircleAvatar(
                          radius: 25,
                          backgroundImage: NetworkImage(sale.photo!),
                        );
                      } else {
                        avatar = const CircleAvatar(
                          radius: 25,
                          child: Icon(Icons.account_circle),
                        );
                      }

                      return ListTile(
                        leading: avatar,
                        title: Text(sale.clientName!),
                        subtitle: Text(sale.address!),
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            ((sale.phone != null && sale.phone!.isNotEmpty)
                                ? IconButton(
                                    icon: const Icon(Icons.phone),
                                    onPressed: () {
                                      // Realizar la llamada Telefonica
                                      var url = Uri(
                                        scheme: "tel",
                                        path: sale.phone,
                                      );
                                      // Abrir con Whatsapp
                                      // var url = Uri(
                                      //   scheme: "https",
                                      //   path:
                                      //       "wa.me/${sale.phone}/?text=Hola+Mundo",
                                      // );
                                      launchUrl(url);
                                    },
                                  )
                                : const Text("")),
                            (sale.lat != null
                                ? IconButton(
                                    icon: const Icon(Icons.pin_drop_rounded),
                                    onPressed: () {
                                      // mostrar ruta hasta el lugar
                                      // var url = Uri(
                                      //   scheme: "google.navigation",
                                      //   path:
                                      //       "q=${sale.lat},${sale.lng}&mode=d",
                                      // );
                                      var url = Uri(
                                        scheme: "geo",
                                        path: "${sale.lat},${sale.lng}",
                                      );
                                      launchUrl(url);
                                    },
                                  )
                                : const Text(""))
                          ],
                        ),
                        onTap: () {
                          // TODO: Ir a la ventana detalle del cobro
                        },
                      );
                    }),
              )
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add_shopping_cart),
          onPressed: () async {
            await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const NewTravel(),
              ),
            );

            if (!mounted) return;

            _listarCobros();
          },
        ),
      ),
    );
  }

  void _listarCobros() {
    _pref.then((pref) {
      var id = pref.getString("uid") ?? "";
      _saleController.listAll(id).then((value) {
        setState(() {
          _lista = value;
        });
      });
    });
  }
}
