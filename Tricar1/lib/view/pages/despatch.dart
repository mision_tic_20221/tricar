import 'package:flutter/material.dart';

import '../widgets/drawer.dart';

class DespatchPage extends StatelessWidget {
  const DespatchPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purple,
        title: const Text("Domicilio"),
      ),
      drawer: const DrawerWidget(),
    );
  }
}
