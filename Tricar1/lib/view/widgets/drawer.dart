import 'package:flutter/material.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tricar_1/view/pages/despatch.dart';
import 'package:tricar_1/view/pages/home.dart';
import 'package:tricar_1/view/pages/othercity.dart';

import 'package:tricar_1/view/widgets/photo_avatar.dart';
import '../../controller/login.dart';
import '../pages/cash_close.dart';
import '../pages/login.dart';
import '../pages/rentcar.dart';
import '../pages/contact.dart';
import '../pages/mytravel.dart';
import 'package:tricar_1/view/pages/map_inside_listview.dart';

class DrawerWidget extends StatefulWidget {
  const DrawerWidget({super.key});
/*
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: const BoxDecoration(
              color: Colors.purple,
            ),
            child: _header(),
          ),
          ListTile(
            leading: const Icon(Icons.add_location_sharp),
            title: const Text('Mis viajes'),
            onTap: () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MytravelPage(
                      email: email,
                      name: name,
                    ),
                  ));
            },
          ),
          ListTile(
            leading: const Icon(Icons.hail_sharp),
            title: const Text('Nueva Carrera'),
            onTap: () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HomePage(
                      email: email,
                      name: name,
                    ),
                  ));
            },
          ),
          ListTile(
            leading: const Icon(Icons.emoji_transportation),
            title: const Text('A otra Ciudad'),
            onTap: () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const OtherCityPage(),
                  ));
            },
          ),
          ListTile(
            leading: const Icon(Icons.motorcycle_rounded),
            title: const Text('Domicilio'),
            onTap: () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const DespatchPage(),
                  ));
            },
          ),
          ListTile(
            leading: const Icon(Icons.car_rental),
            title: const Text('Alquila Carro'),
            onTap: () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const RentcarPage(),
                  ));
            },
          ),
          ListTile(
            leading: const Icon(Icons.quick_contacts_mail_outlined),
            title: const Text('Contacto'),
            onTap: () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const ContactPage(),
                  ));
            },
          ),
        ],
      ),
    );
  }

  Widget _header() {
    // TODO: Consultar los datos de la cabecera
    const image = Icon(Icons.manage_accounts);

    return Row(
      children: [
        const CircleAvatar(
          radius: 30,
          backgroundColor: Color.fromARGB(255, 255, 255, 255),
          foregroundColor: Colors.black,
          child: image,
        ),
        const SizedBox(width: 8),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                name,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                ),
              ),
              const SizedBox(height: 8),
              Text(
                email,
                style: const TextStyle(
                  color: Colors.white,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}*/

  @override
  State<DrawerWidget> createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  final _pref = SharedPreferences.getInstance();
  final _loginController = LoginController();
  final _avatar = PhotoAvatarWidget();
  String _uid = "";
  String _name = "";
  String _email = "";
  bool _isAdmin = false;

  @override
  void initState() {
    super.initState();

    _avatar.action = (photo) async {
      // Actualice la foto en la base de datos
      photo = await _loginController.updatePhoto(_uid, photo);

      var pref = await _pref;
      pref.setString("photo", photo);
    };

    _pref.then((pref) {
      setState(() {
        _uid = pref.getString("uid") ?? "";
        _name = pref.getString("name") ?? "N/A";
        _email = pref.getString("email") ?? "N/A";
        _isAdmin = pref.getBool("admin") ?? false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            // ignore: prefer_const_constructors
            decoration: BoxDecoration(
              color: Colors.purple,
            ),
            child: _header(),
          ),
          ListTile(
            leading: const Icon(Icons.add_location_sharp),
            title: const Text('Mis Viajes'),
            onTap: () {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => const HomePage()),
              );
            },
          ),
          ListTile(
            leading: const Icon(Icons.hail_sharp),
            title: const Text('Nueva Carrera'),
            onTap: () {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => const MapInsideListViewPage()),
              );
            },
          ),
          ListTile(
            leading: const Icon(Icons.shopping_bag),
            title: const Text('Ventas'),
            onTap: () {},
          ),
          ListTile(
            leading: const Icon(Icons.supervisor_account),
            title: const Text('Clientes'),
            onTap: () {},
          ),
          if (_isAdmin)
            ListTile(
              leading: const Icon(Icons.medication_rounded),
              title: const Text('Productos'),
              onTap: () {},
            ),
          ListTile(
            leading: const Icon(Icons.money),
            title: const Text('Cierre de caja'),
            onTap: () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const CashClosePage(),
                  ));
            },
          ),
          ListTile(
            leading: const Icon(Icons.exit_to_app),
            title: const Text('Cerrar sesion'),
            onTap: () async {
              var nav = Navigator.of(context);

              // Cerrar sesion en Auth de Firebase
              _loginController.logout();

              // Limpiar las preferences
              var pref = await _pref;
              pref.remove("uid");
              pref.remove("email");
              pref.remove("name");
              pref.remove("admin");
              pref.remove("photo");

              // Volver a pagina de login
              nav.pushReplacement(
                MaterialPageRoute(builder: (context) => LoginPage()),
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _header() {
    // Consultar los datos de la cabecera
    return Row(
      children: [
        _avatar,
        const SizedBox(width: 8),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                _name,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                ),
              ),
              const SizedBox(height: 8),
              Text(
                _email,
                style: const TextStyle(
                  color: Colors.white,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
